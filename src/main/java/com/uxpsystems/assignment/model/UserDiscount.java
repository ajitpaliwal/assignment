package com.uxpsystems.assignment.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author ajit.paliwal
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDiscount implements Serializable {

	/** The name */
	private String type;
	/** The name */
	private int minAmount;
	/** The name */
	private int maxAmount;
	/** The username */
	private int discountRate;
	/** The password */
}
