package com.uxpsystems.assignment.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;

import com.uxpsystems.assignment.dao.UserDiscountRepository;

import com.uxpsystems.assignment.model.UserDiscount;
import com.uxpsystems.assignment.pojo.Discount;

import com.uxpsystems.assignment.utils.Constants;

/**
 * 
 * @author ajit.paliwal
 *
 */
@Service
@CacheConfig(cacheNames = "configuration")
public class UserDiscountService {
	/**
	 * The LOGGER.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(UserDiscountService.class);

	@Autowired
	private UserDiscountRepository userDiscountRepository;

	/**
	 * 
	 * @param userDiscount
	 * @return
	 */
	@Transactional
	public UserDiscount addDiscount(UserDiscount userDiscount) {
		Discount data = new Discount();
		data.setDiscountRate(userDiscount.getDiscountRate());
		data.setMaxAmount(userDiscount.getMaxAmount());
		data.setMinAmount(userDiscount.getMinAmount());
		data.setRole(userDiscount.getType());
		data.setCreatedOn(new Date());
		userDiscountRepository.save(data);
		return userDiscount;
	}

	/**
	 * 
	 * @param amount
	 * @param type
	 * @return
	 */
	@Cacheable()
	public List<UserDiscount> getDiscount(Integer amount, String type) {
		List<UserDiscount> userdetails = new ArrayList<UserDiscount>();
		List<Discount> details = userDiscountRepository.findByRoleAndMinAmountLessThanQuery(type, amount);

		if (!CollectionUtils.isEmpty(details)) {
			for (Discount discount : details) {
				UserDiscount userDiscount = new UserDiscount();
				userDiscount.setDiscountRate(discount.getDiscountRate());
				userDiscount.setMaxAmount(discount.getMaxAmount());
				userDiscount.setMinAmount(discount.getMinAmount());
				userDiscount.setType(discount.getRole());
				userdetails.add(userDiscount);
			}

		}

		return userdetails;
	}
}
