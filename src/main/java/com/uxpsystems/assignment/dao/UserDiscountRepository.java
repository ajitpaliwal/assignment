package com.uxpsystems.assignment.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import org.springframework.stereotype.Repository;

import com.uxpsystems.assignment.pojo.Discount;

/**
 * 
 * @author ajit.paliwal
 * 
 */
@Repository("userDiscountRepository")
public interface UserDiscountRepository extends JpaRepository<Discount, Integer> {
	/**
	 * 
	 * @param type
	 * @param amount
	 * @return
	 */
	@Query("select c from Discount c where c.role  = ?1 and c.minAmount < ?2 order by c.maxAmount desc")
	List<Discount> findByRoleAndMinAmountLessThanQuery(String type, int amount);

}
