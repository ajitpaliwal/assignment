package com.uxpsystems.assignment.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.uxpsystems.assignment.utils.Constants;

import com.uxpsystems.assignment.model.ResponseData;

import com.uxpsystems.assignment.model.UserDiscount;

import com.uxpsystems.assignment.service.UserDiscountService;

/**
 * 
 * @author ajit paliwal
 *
 */
@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class UserDiscountController {
	/**
	 * The LOGGER.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(UserDiscountController.class);
	/**
	 * The Constant ADD DISCOUNT.
	 */
	private static final String ADD_USER_DISCOUNT = "/discount";
	/**
	 * The Constant GET_DISCOUNT_DETAILS.
	 */
	private static final String GET_DISCOUNT_DETAILS = "/discount/details/{amount}/{type}";

	@Autowired
	private UserDiscountService userDiscountService;

	/**
	 * add discount
	 *
	 * @param userDiscount the userDiscount
	 * @return the userDiscount
	 */
	@PostMapping(path = ADD_USER_DISCOUNT)
	public ResponseEntity<?> addDiscountRate(@RequestBody UserDiscount userDiscount) {
		LOGGER.info("New user registration :- " + userDiscount);
		ResponseData data = new ResponseData();
		try {
			UserDiscount returnData = userDiscountService.addDiscount(userDiscount);
			data.setData(returnData);
			data.setMessage(Constants.NEW_USER);
			data.setStatusCode(Constants.SUCCESS_CODE);
		} catch (Exception e) {
			LOGGER.error("Unable to save new discount slot :- " + e.toString());
			data.setMessage(Constants.UNABLE_TO_PROCESS);
			data.setStatusCode(Constants.ERROR_CODE);
		}
		return new ResponseEntity<ResponseData>(data, HttpStatus.OK);
	}

	/**
	 * Send User Data
	 *
	 * @param amount
	 * @param type
	 * @return the discounted value
	 */
	@GetMapping(path = GET_DISCOUNT_DETAILS)
	public ResponseEntity<?> getUserDiscount(@PathVariable(("amount")) Integer amount,
			@PathVariable(("type")) String type) {
		LOGGER.info("get new user details :- " + amount);
		ResponseData data = new ResponseData();
		try {
			List<UserDiscount> discount = userDiscountService.getDiscount(amount, type);
			double disc = 0.00;
			int totalAmount = amount;
			for (UserDiscount discount2 : discount) {
				double discou = getDiscount(discount2, amount);
				int amt = amount - discount2.getMinAmount();
				amount = amount - amt;
				disc = disc + discou;
			}
			data.setData(totalAmount - disc);
			data.setMessage(Constants.SUCCESS);
			data.setStatusCode(Constants.SUCCESS_CODE);
		} catch (Exception e) {
			LOGGER.error("Unable to find rate :- " + e.toString());
			data.setMessage(Constants.UNABLE_TO_PROCESS);
			data.setStatusCode(Constants.ERROR_CODE);
		}
		return new ResponseEntity<ResponseData>(data, HttpStatus.OK);
	}

	/**
	 * 
	 * @param discount2
	 * @param amount
	 * @return
	 */
	private double getDiscount(UserDiscount discount2, int amount) {
		// TODO Auto-generated method stub

		double disc = (amount - discount2.getMinAmount()) * discount2.getDiscountRate() / 100;
		return disc;
	}

}
